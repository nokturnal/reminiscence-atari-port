/* REminiscence - Flashback interpreter
 * Copyright (C) 2005-2007 Gregory Montoir
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "file.h"
#include "unpack.h"
#include "resource.h"

#ifdef TOS
#include <mint/osbind.h>
#define VOICEPATH "DATA\\VOICE"
#endif

Resource::Resource(const char *dataPath, Version ver) {
	memset(this, 0, sizeof(Resource));
	_dataPath = dataPath;
	_ver = ver;
	#ifdef TOS
	_memBuf = (uint8 *)Mxalloc(0xE000,3L);
	_voiceBuf=NULL;
	#else
	_memBuf = (uint8 *)malloc(0xE000);
	#endif
}

Resource::~Resource() {
	clearLevelRes();
	#ifdef TOS
	Mfree(_fnt);
	Mfree(_icn);
	Mfree(_tab);
	Mfree(_spr1);
	Mfree(_memBuf);
	Mfree(_cmd);
	Mfree(_pol);
	Mfree(_cine_off);
	Mfree(_cine_txt);

	for (int i = 0; i < _numSfx; ++i) {
		Mfree(_sfxList[i].data);
	}
	Mfree(_sfxList);
	Mfree(_voiceBuf);
	#else

        free(_fnt);
	free(_icn);
	free(_tab);
	free(_spr1);
	free(_memBuf);
	free(_cmd);
	free(_pol);
	free(_cine_off);
	free(_cine_txt);

	for (int i = 0; i < _numSfx; ++i) {
		free(_sfxList[i].data);
	}
	free(_sfxList);
	free(_voiceBuf);
	#endif
}

void Resource::clearLevelRes() {
#ifdef TOS
	Mfree(_tbn); _tbn = 0;
	Mfree(_mbk); _mbk = 0;
	Mfree(_mbkData); _mbkData = 0;
	Mfree(_pal); _pal = 0;
	Mfree(_map); _map = 0;
	Mfree(_spc); _spc = 0;
	Mfree(_ani); _ani = 0;
	free_OBJ();
#else
	free(_tbn); _tbn = 0;
	free(_mbk); _mbk = 0;
	free(_mbkData); _mbkData = 0;
	free(_pal); _pal = 0;
	free(_map); _map = 0;
	free(_spc); _spc = 0;
	free(_ani); _ani = 0;
	free_OBJ();
	#endif
}

void Resource::load_FIB(const char *fileName) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_FIB('%s')", fileName);
	#endif
	static const uint8 fibonacciTable[] = {
		0xDE, 0xEB, 0xF3, 0xF8, 0xFB, 0xFD, 0xFE, 0xFF,
		0x00, 0x01, 0x02, 0x03, 0x05, 0x08, 0x0D, 0x15
	};
	sprintf(_entryName, "%s.FIB", fileName);
	File f;
	if (f.open(_entryName, _dataPath, "rb")) {
		_numSfx = f.readUint16LE();
		#ifdef TOS
		_sfxList = (SoundFx *)Mxalloc(_numSfx * sizeof(SoundFx),3L);
		#else
		_sfxList = (SoundFx *)malloc(_numSfx * sizeof(SoundFx));
		#endif
                if (!_sfxList) {
			error("Unable to allocate SoundFx table");
		}
		int i;
		for (i = 0; i < _numSfx; ++i) {
			SoundFx *sfx = &_sfxList[i];
			sfx->offset = f.readUint32LE();
			sfx->len = f.readUint16LE();
			sfx->data = 0;
		}
		for (i = 0; i < _numSfx; ++i) {
			SoundFx *sfx = &_sfxList[i];
			if (sfx->len == 0) {
				continue;
			}
			f.seek(sfx->offset);
			#ifdef TOS
			uint8 *data = (uint8 *)Mxalloc(sfx->len * 2,3L);
			#else
                        uint8 *data = (uint8 *)malloc(sfx->len * 2);
			#endif
                        if (!data) {
				error("Unable to allocate SoundFx data buffer");
			}
			sfx->data = data;
			uint8 c = f.readByte();
			*data++ = c;
			*data++ = c;
			uint16 sz = sfx->len - 1;
			while (sz--) {
				uint8 d = f.readByte();
				c += fibonacciTable[d >> 4];
				*data++ = c;
				c += fibonacciTable[d & 15];
				*data++ = c;
			}
			sfx->len *= 2;
		}
		if (f.ioErr()) {
			error("I/O error when reading '%s'", _entryName);
		}
	} else {
		error("Can't open '%s'", _entryName);
	}
}

void Resource::load_MAP_menu(const char *fileName, uint8 *dstPtr) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_MAP_menu('%s')", fileName);
	#endif
	sprintf(_entryName, "%s.MAP", fileName);
	File f;
	if (f.open(_entryName, _dataPath, "rb")) {
		if (f.size() != 0x3800 * 4) {
			error("Wrong file size for '%s', %d", _entryName, f.size());
		}
		f.read(dstPtr, 0x3800 * 4);
		if (f.ioErr()) {
			error("I/O error when reading '%s'", _entryName);
		}
	} else {
		error("Can't open '%s'", _entryName);
	}
}

void Resource::load_PAL_menu(const char *fileName, uint8 *dstPtr) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_PAL_menu('%s')", fileName);
	#endif
	sprintf(_entryName, "%s.PAL", fileName);
	File f;
	if (f.open(_entryName, _dataPath, "rb")) {
		if (f.size() != 768) {
			error("Wrong file size for '%s', %d", _entryName, f.size());
		}
		f.read(dstPtr, 768);
		if (f.ioErr()) {
			error("I/O error when reading '%s'", _entryName);
		}
	} else {
		error("Can't open '%s'", _entryName);
	}
}

void Resource::load_SPR_OFF(const char *fileName, uint8 *sprData) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_SPR_OFF('%s')", fileName);
	#endif
	sprintf(_entryName, "%s.OFF", fileName);
	File f;
	if (f.open(_entryName, _dataPath, "rb")) {
		int len = f.size();
		#ifdef TOS
		uint8 *offData = (uint8 *)Mxalloc(len,3L);
		#else
		uint8 *offData = (uint8 *)malloc(len);
		#endif
		if (!offData) {
			error("Unable to allocate sprite offsets");
		}
		f.read(offData, len);
		if (f.ioErr()) {
			error("I/O error when reading '%s'", _entryName);
		}
		const uint8 *p = offData;
		uint16 pos;
		while ((pos = READ_LE_UINT16(p)) != 0xFFFF) {
			uint32 off = READ_LE_UINT32(p + 2);
			if (off == 0xFFFFFFFF) {
				_spr_off[pos] = 0;
			} else {
				_spr_off[pos] = sprData + off;
			}
			p += 6;
		}
		#ifdef TOS
		Mfree(offData);
		#else
		free(offData);
		#endif
	} else {
		error("Can't open '%s'", _entryName);
	}
}

void Resource::load_CINE() {
	const char *baseName = 0;
	switch (_ver) {
	case VER_FR:
		baseName = "FR_CINE";
		break;
	case VER_EN:
		baseName = "ENGCINE";
		break;
	case VER_DE:
		baseName = "GERCINE";
		break;
	case VER_SP:
		baseName = "SPACINE";
		break;
	}
	#ifndef NDEBUG
	debug(DBG_RES, "Resource::load_CINE('%s')", baseName);
	#endif
	if (_cine_off == 0) {
		sprintf(_entryName, "%s.BIN", baseName);
		File f;
		if (f.open(_entryName, _dataPath, "rb")) {
			int len = f.size();
			#ifdef TOS
			_cine_off = (uint8 *)Mxalloc(len,3L);
			#else
			_cine_off = (uint8 *)malloc(len);
			#endif
                        if (!_cine_off) {
				error("Unable to allocate cinematics offsets");
			}
			f.read(_cine_off, len);
			if (f.ioErr()) {
				error("I/O error when reading '%s'", _entryName);
			}
		} else {
			error("Can't open '%s'", _entryName);
		}
	}
	if (_cine_txt == 0) {
		sprintf(_entryName, "%s.TXT", baseName);
		File f;
		if (f.open(_entryName, _dataPath, "rb")) {
			int len = f.size();
			#ifdef TOS
			_cine_txt = (uint8 *)Mxalloc(len,3L);
			#else
			_cine_txt = (uint8 *)malloc(len);
			#endif
                        if (!_cine_txt) {
				error("Unable to allocate cinematics text data");
			}
			f.read(_cine_txt, len);
			if (f.ioErr()) {
				error("I/O error when reading '%s'", _entryName);
			}
		} else {
			error("Can't open '%s'", _entryName);
		}
	}
}

void Resource::load_TEXT() {
	File f;
	// Load game strings
	_stringsTable = 0;
	if (f.open("STRINGS.TXT", _dataPath, "rb")) {
		const int sz = f.size();
		#ifdef TOS
		_extStringsTable = (uint8 *)Mxalloc(sz,3L);
		#else
                _extStringsTable = (uint8 *)malloc(sz);
		#endif
		if (_extStringsTable) {
			f.read(_extStringsTable, sz);
			_stringsTable = _extStringsTable;
		}
		f.close();
	}
	if (!_stringsTable) {
		switch (_ver) {
		case VER_FR:
			_stringsTable = LocaleData::_stringsTableFR;
			break;
		case VER_EN:
			_stringsTable = LocaleData::_stringsTableEN;
			break;
		case VER_DE:
			_stringsTable = LocaleData::_stringsTableDE;
			break;
		case VER_SP:
			_stringsTable = LocaleData::_stringsTableSP;
			break;
		}
	}
	// Load menu strings
	_textsTable = 0;
	if (f.open("MENUS.TXT", _dataPath, "rb")) {
		const int offs = LocaleData::LI_NUM * sizeof(char *);
		const int sz = f.size() + 1;
		#ifdef TOS
		_extTextsTable = (char **)Mxalloc(offs + sz,3L);
		#else
		_extTextsTable = (char **)malloc(offs + sz);
		#endif
                if (_extTextsTable) {
			char *textData = (char *)_extTextsTable + offs;
			f.read(textData, sz);
			textData[sz] = 0;
			int textsCount = 0;
			for (char *eol; (eol = strpbrk(textData, "\r\n")) != 0; ) {
				*eol++ = 0;
				if (*eol == '\r' || *eol == '\n') {
					*eol++ = 0;
				}
				if (textsCount < LocaleData::LI_NUM && textData[0] != 0) {
					_extTextsTable[textsCount] = textData;
					++textsCount;
				}
				textData = eol;
			}
			if (textsCount < LocaleData::LI_NUM && textData[0] != 0) {
				_extTextsTable[textsCount] = textData;
				++textsCount;
			}
			if (textsCount < LocaleData::LI_NUM) {
                          #ifdef TOS
                          Mfree(_extTextsTable);
                          #else
				free(_extTextsTable);
				#endif
                                _extTextsTable = 0;
			} else {
				_textsTable = (const char **)_extTextsTable;
			}
		}
	}
	if (!_textsTable) {
		switch (_ver) {
		case VER_FR:
			_textsTable = LocaleData::_textsTableFR;
			break;
		case VER_EN:
			_textsTable = LocaleData::_textsTableEN;
			break;
		case VER_DE:
			_textsTable = LocaleData::_textsTableDE;
			break;
		case VER_SP:
			_textsTable = LocaleData::_textsTableSP;
			break;
		}
	}
}

void Resource::free_TEXT() {
	if (_extTextsTable) {
          #ifdef TOS
            	Mfree(_extTextsTable);
          #else
		free(_extTextsTable);
	#endif
                _extTextsTable = 0;
	}
	_stringsTable = 0;
	if (_extStringsTable) {
          #ifdef TOS
               Mfree(_extStringsTable);
        #else
        	free(_extStringsTable);
	#endif
                _extStringsTable = 0;
	}
	_textsTable = 0;
}

void Resource::load(const char *objName, int objType) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load('%s', %d)", objName, objType);
	#endif
	LoadStub loadStub = 0;
	switch (objType) {
	case OT_MBK:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement bank (mbk)");
		#endif
		sprintf(_entryName, "%s.MBK", objName);
		loadStub = &Resource::load_MBK;
		break;
	case OT_PGE:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement piege (pge)");
		#endif
		sprintf(_entryName, "%s.PGE", objName);
		loadStub = &Resource::load_PGE;
		break;
	case OT_PAL:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement palettes (pal)");
		#endif
		sprintf(_entryName, "%s.PAL", objName);
		loadStub = &Resource::load_PAL;
		break;
	case OT_CT:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement collision (ct)");
		#endif
		sprintf(_entryName, "%s.CT", objName);
		loadStub = &Resource::load_CT;
		break;
	case OT_MAP:
	#ifndef NDEBUG
		debug(DBG_RES, "ouverture map (map)");
		#endif
		sprintf(_entryName, "%s.MAP", objName);
		loadStub = &Resource::load_MAP;
		break;
	case OT_SPC:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement sprites caracteres (spc)");
		#endif
		strcpy(_entryName, "GLOBAL.SPC");
		loadStub = &Resource::load_SPC;
		break;
	case OT_RP:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement positions des banks pour sprite_car (rp)");
		#endif
		sprintf(_entryName, "%s.RP", objName);
		loadStub = &Resource::load_RP;
		break;
	case OT_SPR:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement des sprites (spr)");
		#endif
		sprintf(_entryName, "%s.SPR", objName);
		loadStub = &Resource::load_SPR;
		break;
	case OT_SPRM:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement des sprites monstre (spr)");
		#endif
		sprintf(_entryName, "%s.SPR", objName);
		loadStub = &Resource::load_SPRM;
		break;
	case OT_ICN:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement des icones (icn)");
		#endif
		sprintf(_entryName, "%s.ICN", objName);
		loadStub = &Resource::load_ICN;
		break;
	case OT_FNT:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement de la font (fnt)");
		#endif
		sprintf(_entryName, "%s.FNT", objName);
		loadStub = &Resource::load_FNT;
		break;
	case OT_OBJ:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement objets (obj)");
		#endif
		sprintf(_entryName, "%s.OBJ", objName);
		loadStub = &Resource::load_OBJ;
		break;
	case OT_ANI:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement animations (ani)");
		#endif
		sprintf(_entryName, "%s.ANI", objName);
		loadStub = &Resource::load_ANI;
		break;
	case OT_TBN:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement des textes (tbn)");
		#endif
		sprintf(_entryName, "%s.TBN", objName);
		loadStub = &Resource::load_TBN;
		break;
	case OT_CMD:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement des commandes (cmd)");
		#endif
		sprintf(_entryName, "%s.CMD", objName);
		loadStub = &Resource::load_CMD;
		break;
	case OT_POL:
	#ifndef NDEBUG
		debug(DBG_RES, "chargement des polygones (pol)");
		#endif
		sprintf(_entryName, "%s.POL", objName);
		loadStub = &Resource::load_POL;
		break;
	default:
	
		error("Unimplemented Resource::load() type %d", objType);
		break;
	}
	if (loadStub) {
		File f;
		if (f.open(_entryName, _dataPath, "rb")) {
			(this->*loadStub)(&f);
			if (f.ioErr()) {
				error("I/O error when reading '%s'", _entryName);
			}
		} else {
			error("Can't open '%s'", _entryName);
		}
	}
}

void Resource::load_CT(File *pf) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_CT()");
	#endif
	int len = pf->size();
	#ifdef TOS
	uint8 *tmp = (uint8 *)Mxalloc(len,3L);
	#else
	uint8 *tmp = (uint8 *)malloc(len);
	#endif
	if (!tmp) {
		error("Unable to allocate CT buffer");
	} else {
		pf->read(tmp, len);
		if (!delphine_unpack((uint8 *)_ctData, tmp, len)) {
			error("Bad CRC for collision data");
		}
		#ifdef TOS
		Mfree(tmp);
                #else
                free(tmp);
                #endif
	}
}

void Resource::load_FNT(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_FNT()");
	#endif
	int len = f->size();
	#ifdef TOS
	_fnt = (uint8 *)Mxalloc(len,3L);
	#else
	_fnt = (uint8 *)malloc(len);
	#endif
	if (!_fnt) {
		error("Unable to allocate FNT buffer");
	} else {
		f->read(_fnt, len);
	}
}

void Resource::load_MBK(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_MBK()");
	#endif
	uint8 num = f->readByte();
	int dataSize = f->size() - num * 6;
	#ifdef TOS
	_mbk = (MbkEntry *)Mxalloc(sizeof(MbkEntry) * num,3L);
	#else
	_mbk = (MbkEntry *)malloc(sizeof(MbkEntry) * num);
	#endif
        if (!_mbk) {
		error("Unable to allocate MBK buffer");
	}
	f->seek(0);
	for (int i = 0; i < num; ++i) {
		f->readUint16BE(); /* unused */
		_mbk[i].offset = f->readUint16BE() - num * 6;
		_mbk[i].len = f->readUint16BE();
		#ifndef NDEBUG
		debug(DBG_RES, "dataSize=0x%X entry %d off=0x%X len=0x%X", dataSize, i, _mbk[i].offset + num * 6, _mbk[i].len);
		#endif
                assert(_mbk[i].offset <= dataSize);
	}
	#ifdef TOS
	_mbkData = (uint8 *)Mxalloc(dataSize,3L);
	#else
	_mbkData = (uint8 *)malloc(dataSize);
	#endif
	if (!_mbkData) {
		error("Unable to allocate MBK data buffer");
	}
	f->read(_mbkData, dataSize);
}

void Resource::load_ICN(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_ICN()");
	#endif
	int len = f->size();
	#ifdef TOS
	_icn = (uint8 *)Mxalloc(len,3L);
	#else
	_icn = (uint8 *)malloc(len);
	#endif
	if (!_icn) {
		error("Unable to allocate ICN buffer");
	} else {
		f->read(_icn, len);
	}
}

void Resource::load_SPR(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_SPR()");
	#endif
	int len = f->size() - 12;
	#ifdef TOS
	_spr1 = (uint8 *)Mxalloc(len,3L);
	#else
	_spr1 = (uint8 *)malloc(len);
	#endif
	if (!_spr1) {
		error("Unable to allocate SPR buffer");
	} else {
		f->seek(12);
		f->read(_spr1, len);
	}
}

void Resource::load_SPRM(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_SPRM()");
	#endif
	int len = f->size() - 12;
	f->seek(12);
	f->read(_sprm, len);
}

void Resource::load_RP(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_RP()");
	#endif
	f->read(_rp, 0x4A);
}

void Resource::load_SPC(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_SPC()");
	#endif
	int len = f->size();
	#ifdef TOS
        _spc = (uint8 *)Mxalloc(len,3);
	#else
	_spc = (uint8 *)malloc(len);
	#endif
	if (!_spc) {
		error("Unable to allocate SPC buffer");
	} else {
		f->read(_spc, len);
		#ifdef TOS
		_numSpc = *((uint16 *)(_spc)) / 2;
		#else
		_numSpc = READ_BE_UINT16(_spc) / 2;
		
		#endif
	}
}

void Resource::load_PAL(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_PAL()");
	#endif
	int len = f->size();
	#ifdef TOS
	_pal = (uint8 *)Mxalloc(len,3);
	#else
	_pal = (uint8 *)malloc(len);
	#endif
	if (!_pal) {
		error("Unable to allocate PAL buffer");
	} else {
		f->read(_pal, len);
	}
}

void Resource::load_MAP(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_MAP()");
	#endif
	int len = f->size();
	#ifdef TOS
	_map = (uint8 *)Mxalloc(len,3L);
	#else
	_map = (uint8 *)malloc(len);
	#endif
	if (!_map) {
		error("Unable to allocate MAP buffer");
	} else {
		f->read(_map, len);
	}
}

void Resource::load_OBJ(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_OBJ()");
	#endif

	uint16 i;

	_numObjectNodes = f->readUint16LE();
	assert(_numObjectNodes < 255);

	uint32 offsets[256];
	for (i = 0; i < _numObjectNodes; ++i) {
		offsets[i] = f->readUint32LE();
	}
	offsets[i] = f->size() - 2;

	int numObjectsCount = 0;
	uint16 objectsCount[256];
	for (i = 0; i < _numObjectNodes; ++i) {
		int diff = offsets[i + 1] - offsets[i];
		if (diff != 0) {
			objectsCount[numObjectsCount] = (diff - 2) / 0x12;
			#ifndef NDEBUG
			debug(DBG_RES, "i=%d objectsCount[numObjectsCount]=%d", i, objectsCount[numObjectsCount]);
			#endif
			++numObjectsCount;
		}
	}

	uint32 prevOffset = 0;
	ObjectNode *prevNode = 0;
	int iObj = 0;
	for (i = 0; i < _numObjectNodes; ++i) {
		if (prevOffset != offsets[i]) {
           #ifdef TOS
                        ObjectNode *on = (ObjectNode *)Mxalloc(sizeof(ObjectNode),3);
           #else
			ObjectNode *on = (ObjectNode *)malloc(sizeof(ObjectNode));
           #endif
			if (!on) {
				error("Unable to allocate ObjectNode num=%d", i);
			}
			f->seek(offsets[i] + 2);
			on->last_obj_number = f->readUint16LE();
			on->num_objects = objectsCount[iObj];
			#ifndef NDEBUG
			debug(DBG_RES, "last=%d num=%d", on->last_obj_number, on->num_objects);
			#endif
			#ifdef TOS
                        on->objects = (Object *)Mxalloc(sizeof(Object) * on->num_objects,3);
                        #else
			on->objects = (Object *)malloc(sizeof(Object) * on->num_objects);
			#endif
                        for (uint16 j = 0; j < on->num_objects; ++j) {
				Object *obj = &on->objects[j];
				obj->type = f->readUint16LE();
				obj->dx = f->readByte();
				obj->dy = f->readByte();
				obj->init_obj_type = f->readUint16LE();
				obj->opcode2 = f->readByte();
				obj->opcode1 = f->readByte();
				obj->flags = f->readByte();
				obj->opcode3 = f->readByte();
				obj->init_obj_number = f->readUint16LE();
				obj->opcode_arg1 = f->readUint16LE();
				obj->opcode_arg2 = f->readUint16LE();
				obj->opcode_arg3 = f->readUint16LE();
				#ifndef NDEBUG
				debug(DBG_RES, "obj_node=%d obj=%d op1=0x%X op2=0x%X op3=0x%X", i, j, obj->opcode2, obj->opcode1, obj->opcode3);
				#endif
			}
			++iObj;
			prevOffset = offsets[i];
			prevNode = on;
		}
		_objectNodesMap[i] = prevNode;
	}
}

void Resource::free_OBJ() {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::free_OBJ()");
	#endif
	ObjectNode *prevNode = 0;
	for (int i = 0; i < _numObjectNodes; ++i) {
		if (_objectNodesMap[i] != prevNode) {
			ObjectNode *curNode = _objectNodesMap[i];
			#ifdef TOS
			Mfree(curNode->objects);
			#else
                        free(curNode->objects);
			#endif
                        _objectNodesMap[i] = 0;
			prevNode = curNode;
		}
	}
}

void Resource::load_PGE(File *f) {
  
  
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_PGE()");
	#endif
	_pgeNum = f->readUint16LE();
	memset(_pgeInit, 0, sizeof(_pgeInit));
	#ifndef NDEBUG
	debug(DBG_RES, "len=%d _pgeNum=%d", len, _pgeNum);
	#endif
	for (uint16 i = 0; i < _pgeNum; ++i) {
		InitPGE *pge = &_pgeInit[i];
		pge->type = f->readUint16LE();
		pge->pos_x = f->readUint16LE();
		pge->pos_y = f->readUint16LE();
		pge->obj_node_number = f->readUint16LE();
		pge->life = f->readUint16LE();
		for (int lc = 0; lc < 4; ++lc) {
			pge->counter_values[lc] = f->readUint16LE();
		}
		pge->object_type = f->readByte();
		pge->init_room = f->readByte();
		pge->room_location = f->readByte();
		pge->init_flags = f->readByte();
		pge->colliding_icon_num = f->readByte();
		pge->icon_num = f->readByte();
		pge->object_id = f->readByte();
		pge->skill = f->readByte();
		pge->mirror_x = f->readByte();
		pge->flags = f->readByte();
		pge->unk1C = f->readByte();
		f->readByte();
		pge->text_num = f->readByte();
		f->readByte();
	}
}

void Resource::load_ANI(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_ANI()");
	#endif
	int size = f->size() - 2;
	#ifdef TOS
	_ani = (uint8 *)Mxalloc(size,3L);
        #else
        _ani = (uint8 *)malloc(size);
	#endif
        if (!_ani) {
		error("Unable to allocate ANI buffer");
	} else {
		f->seek(2);
		f->read(_ani, size);
	}
}

void Resource::load_TBN(File *f) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_TBN()");
	#endif
	int len = f->size();
	#ifdef TOS
	_tbn = (uint8 *)Mxalloc(len,3L);
        #else
	_tbn = (uint8 *)malloc(len);
	#endif
        if (!_tbn) {
		error("Unable to allocate TBN buffer");
	} else {
		f->read(_tbn, len);
	}
}

void Resource::load_CMD(File *pf) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_CMD()");
	#endif
	#ifdef TOS
	Mfree(_cmd);
	#else
        free(_cmd);
	#endif
	int len = pf->size();
	#ifdef TOS
	_cmd = (uint8 *)Mxalloc(len,3L);
        #else
	_cmd = (uint8 *)malloc(len);
	#endif
        if (!_cmd) {
		error("Unable to allocate CMD buffer");
	} else {
		pf->read(_cmd, len);
	}
}

void Resource::load_POL(File *pf) {
  #ifndef NDEBUG
	debug(DBG_RES, "Resource::load_POL()");
	#endif
	#ifdef TOS
	Mfree(_pol);
	#else
	free(_pol);
	#endif
	int len = pf->size();
	#ifdef TOS
	_pol = (uint8 *)Mxalloc(len,3L);
	#else
	_pol = (uint8 *)malloc(len);
	#endif
        if (!_pol) {
		error("Unable to allocate POL buffer");
	} else {
		pf->read(_pol, len);
	}
}

void Resource::load_VCE(int num, int segment, uint8 **buf, uint32 *bufSize) {
	*buf = 0;
	int offset = _voicesOffsetsTable[num];
	if (offset != 0xFFFF) {
		const uint16 *p = _voicesOffsetsTable + offset / 2;
		offset = (*p++) * 2048;
		int count = *p++;
		if (segment < count) {
			File f;
			#ifdef TOS
			if (f.open("VOICE.VCE", VOICEPATH, "rb"))
                        #else
                           if (f.open("VOICE.VCE", _dataPath, "rb"))
                        #endif
                         {
                           #ifndef NDEBUG
				debug(DBG_INFO, "Resource::load_VCE()");
			#endif
                                int voiceSize = p[segment] * 2048 / 5;
				#ifdef TOS
				 if(_voiceBuf!=NULL)
                                 {Mfree(_voiceBuf); 
                                  _voiceBuf=NULL;
                                 }


				_voiceBuf = (uint8 *)Mxalloc(voiceSize,3L);
				#else
                                free(_voiceBuf);
				_voiceBuf = (uint8 *)malloc(voiceSize);
				#endif
                                if (_voiceBuf) {
					uint8 *dst = _voiceBuf;
					offset += 0x2000;
					for (int s = 0; s < count; ++s) {
						int len = p[s] * 2048;
						for (int i = 0; i < len / (0x2000 + 2048); ++i) {
							if (s == segment) {
								f.seek(offset);
								int n = 2048;
								while (n--) {
									int v = f.readByte();
									if (v & 0x80) {
										v = -(v & 0x7F);
									}
									*dst++ = (uint8)(v & 0xFF);
								}
							}
							offset += 0x2000 + 2048;
						}
						if (s == segment) {
							break;
						}
					}
					*buf = _voiceBuf;
					*bufSize = voiceSize;
				}
			}
		}
	}
}
