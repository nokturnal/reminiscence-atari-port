/* REminiscence - Flashback interpreter
 * Copyright (C) 2005-2007 Gregory Montoir
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <SDL.h>
#include <cstdio>

#include "scaler.h"
#include "systemstub.h"
#include "game.h"
#include "save_tga.h"

#ifdef TOS
//jagpad enums
extern "C"
{
 enum{
     IKBD_JOY1=0,  //standard
     JAGPAD_A_0=1, //standard
     JAGPAD_A_1=2, //TeamTap only
     JAGPAD_A_2=3, //TeamTap only
     JAGPAD_A_3=4, //TeamTap only
     JAGPAD_B_0=5, //standard
     JAGPAD_B_1=6, //TeamTap only
     JAGPAD_B_2=7, //TeamTap only
     JAGPAD_B_3=8  //TeamTap only
  } eAtariJoysticks;

/* Joypad buttons
 * Procontroller note:
 * L,R are connected to 4,6
 * X,Y,Z are connected to 7,8,9
 */
 enum{
  JP_FIRE0=0, /*button A*/JP_FIRE1, /*button B*/JP_FIRE2, /*button C*/JP_PAUSE, /*button PAUSE*/
  JP_OPTION, /*button PAUSE */JP_KPMULT, /*button ASTERIX*/JP_KPNUM, /*button hash */JP_KP0,
  JP_KP1,JP_KP2,JP_KP3,JP_KP4,JP_KP5,JP_KP6,JP_KP7,JP_KP8, JP_KP9
 } eJapadButtons;
};

#endif


void SystemStub::init(const char *title, uint16 w, uint16 h) {
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK);
	SDL_ShowCursor(SDL_DISABLE);
	SDL_WM_SetCaption(title, NULL);
	memset(&_pi, 0, sizeof(_pi));
	_screenW = w;
	_screenH = h;
	pDesired=NULL;
	pObtained=NULL;
        _iNumScreenshots=0;

        // allocate some extra bytes for the scaling routines
	int size_offscreen = (w + 2) * (h + 2) * 2;
	#ifdef TOS
         _offscreen = (uint8 *)Mxalloc(size_offscreen,3L);
	#else
	_offscreen = (uint8 *)malloc(size_offscreen);
	#endif

        if (!_offscreen) {
		error("SystemStub::init() Unable to allocate offscreen buffer");
	}
	memset(_offscreen, 0, size_offscreen);

	#ifdef TOS
	_fullscreen = 1;
	_scaler = 0;	//scaler off by default
        #else
	_fullscreen = FULLSCREEN;
	_scaler = SCALER_INI;
        #endif
	memset(_pal, 0, sizeof(_pal));
	prepareGfxMode();

         iNumJoysticks=SDL_NumJoysticks();

         //clear joystick array
         for (int i=0;i<MAX_JOY;i++)
         {
         _joystick[i]=NULL;
         }


        if (iNumJoysticks > 0) {
          #ifndef NDEBUG
          debug(DBG_INFO,"%d joysticks found\n", iNumJoysticks);
          #endif

          #ifdef TOS
           //open IKBD joy port 1 and two jagpads, without TeamTap
            if(openJoystick(IKBD_JOY1, _joystick[IKBD_JOY1]))
            {
             debug(DBG_INFO,"%s opened...",SDL_JoystickName(IKBD_JOY1));
            }
             else
              {
               debug(DBG_INFO,"%s not opened...",SDL_JoystickName(IKBD_JOY1));
              }

            if(openJoystick(JAGPAD_A_0, _joystick[JAGPAD_A_0]))
            {
             debug(DBG_INFO, "%s opened...",SDL_JoystickName(JAGPAD_A_0));
            }
             else
              {debug(DBG_INFO, "%s not opened...",SDL_JoystickName(JAGPAD_A_0));}

             if(openJoystick(JAGPAD_B_0, _joystick[JAGPAD_B_0]))
             {
              debug(DBG_INFO, "%s opened...",SDL_JoystickName(JAGPAD_B_0));
             }
              else
              {
                 debug(DBG_INFO, "%s not opened...",SDL_JoystickName(JAGPAD_B_0));
              }
          #else
             for(int i=0;(i<iNumJoysticks);i++)
                     {
		       _joystick[i] = SDL_JoystickOpen(i);
		       if(_joystick[i]){
                       #ifndef NDEBUG
			debug(DBG_INFO,"Opened Joystick %d\n",i);
                        debug(DBG_INFO,"Name: %s\n", SDL_JoystickName(i));
                        debug(DBG_INFO,"Number of Axes: %d\n", SDL_JoystickNumAxes(_joystick[i]));
                        debug(DBG_INFO,"Number of Buttons: %d\n", SDL_JoystickNumButtons(_joystick[i]));
                        debug(DBG_INFO,"Number of Balls: %d\n", SDL_JoystickNumBalls(_joystick[i]));
                       #endif
                       }else{
                        #ifndef NDEBUG
                        debug(DBG_INFO,"Couldn't open Joystick %d\n",i);
                        #endif
                        _joystick[i]=NULL;
                       }
                      }
          #endif
	}
}

void SystemStub::destroy() {
	cleanupGfxMode();
        if(pDesired!=NULL)
        {free(pDesired);}
	if(pObtained!=NULL)
	{free(pObtained);}

           for(int i=0;i<iNumJoysticks;i++)
           {
                if(_joystick[i]!=NULL){
                     if (SDL_JoystickOpened(i))
                     {SDL_JoystickClose(_joystick[i]); _joystick[i]=NULL;}
                }
           }

        SDL_ShowCursor(SDL_ENABLE);
	SDL_Quit();
}

void SystemStub::setPalette(const uint8 *pal, uint16 n) {
	assert(n <= 256);
	for (int i = 0; i < n; ++i) {
		uint8 r = pal[i * 3 + 0];
		uint8 g = pal[i * 3 + 1];
		uint8 b = pal[i * 3 + 2];
		_pal[i] = SDL_MapRGB(_screen->format, r, g, b);
	}
}

void SystemStub::setPaletteEntry(uint8 i, const Color *c) {
	uint8 r = (c->r << 2) | (c->r & 3);
	uint8 g = (c->g << 2) | (c->g & 3);
	uint8 b = (c->b << 2) | (c->b & 3);
	_pal[i] = SDL_MapRGB(_screen->format, r, g, b);
}

void SystemStub::getPaletteEntry(uint8 i, Color *c) {
	SDL_GetRGB(_pal[i], _screen->format, &c->r, &c->g, &c->b);
	c->r >>= 2;
	c->g >>= 2;
	c->b >>= 2;
}

void SystemStub::setOverscanColor(uint8 i) {
	_overscanColor = i;
}

void SystemStub::copyRect(int16 x, int16 y, uint16 w, uint16 h, const uint8 *buf, uint32 pitch) {
	if (_numBlitRects >= MAX_BLIT_RECTS) {
          #ifndef NDEBUG
		warning("SystemStub::copyRect() Too many blit rects, you may experience graphical glitches");
	  #endif
	} else {
		// extend the dirty region by 1 pixel for scalers accessing 'outer' pixels
		--x;
		--y;
		w += 2;
		h += 2;

		if (x < 0) {
			x = 0;
		}
		if (y < 0) {
			y = 0;
		}
		if (x + w > _screenW) {
			w = _screenW - x;
		}
		if (y + h > _screenH) {
			h = _screenH - y;
		}

		SDL_Rect *br = &_blitRects[_numBlitRects];

		br->x = _pi.mirrorMode ? _screenW - (x + w) : x;
		br->y = y;
		br->w = w;
		br->h = h;
		++_numBlitRects;

		uint16 *p = (uint16 *)_offscreen + (br->y + 1) * _screenW + (br->x + 1);
		buf += y * pitch + x;

		if (_pi.mirrorMode) {
			while (h--) {
				for (int i = 0; i < w; ++i) {
					p[i] = _pal[buf[w - 1 - i]];
				}
				p += _screenW;
				buf += pitch;
			}
		} else {
			while (h--) {
				for (int i = 0; i < w; ++i) {
					p[i] = _pal[buf[i]];
				}
				p += _screenW;
				buf += pitch;
			}
		}
		if (_pi.dbgMask & PlayerInput::DF_DBLOCKS) {
			drawRect(br, 0xE7, (uint16 *)_offscreen + _screenW + 1, _screenW * 2);
		}
	}
}

void SystemStub::updateScreen(uint8 shakeOffset) {
	const int mul = _scalers[_scaler].factor;
	if (shakeOffset == 0) {
		for (int i = 0; i < _numBlitRects; ++i) {
		  SDL_Rect *br = &_blitRects[i];
		  int16 dx = br->x * mul;
		  int16 dy = br->y * mul;
		  
		  SDL_LockSurface(_sclscreen);
		  
		  uint16 *dst = (uint16 *)_sclscreen->pixels + dy * _sclscreen->pitch / 2 + dx;
		  const uint16 *src = (uint16 *)_offscreen + (br->y + 1) * _screenW + (br->x + 1);
		  (*_scalers[_scaler].proc)(dst, _sclscreen->pitch, src, _screenW, br->w, br->h);
		  SDL_UnlockSurface(_sclscreen);
		
		  br->x *= mul;
		  br->y *= mul;
		  br->w *= mul;
		  br->h *= mul;
		  SDL_BlitSurface(_sclscreen, br, _screen, br);
		}
		SDL_UpdateRects(_screen, _numBlitRects, _blitRects);
	} else {
		SDL_LockSurface(_sclscreen);
		uint16 w = _screenW;
		uint16 h = _screenH - shakeOffset;
		uint16 *dst = (uint16 *)_sclscreen->pixels;
		const uint16 *src = (uint16 *)_offscreen + _screenW + 1;
		(*_scalers[_scaler].proc)(dst, _sclscreen->pitch, src, _screenW, w, h);
		SDL_UnlockSurface(_sclscreen);

		SDL_Rect bsr, bdr;
		bdr.x = 0;
		bdr.y = 0;
		bdr.w = _screenW * mul;
		bdr.h = shakeOffset * mul;
		SDL_FillRect(_screen, &bdr, _pal[_overscanColor]);

		bsr.x = 0;
		bsr.y = 0;
		bsr.w = _screenW * mul;
		bsr.h = (_screenH - shakeOffset) * mul;
		bdr.x = 0;
		bdr.y = shakeOffset * mul;
		bdr.w = bsr.w;
		bdr.h = bsr.h;
		
		SDL_BlitSurface(_sclscreen, &bsr, _screen, &bdr);

		bdr.x = 0;
		bdr.y = 0;
		bdr.w = _screenW * mul;
		bdr.h = _screenH * mul;
		SDL_UpdateRects(_screen, 1, &bdr);
	}
	_numBlitRects = 0;
}

void SystemStub::processEvents() {
	SDL_Event ev;
	while (SDL_PollEvent(&ev)) {

                switch (ev.type)
                {
		       case SDL_QUIT:
			_pi.quit = true;
			break;
			//jagpads
		      case SDL_JOYHATMOTION:
			_pi.dirMask = 0;
			if (ev.jhat.value & SDL_HAT_UP) {
				_pi.dirMask |= PlayerInput::DIR_UP;
			}
			if (ev.jhat.value & SDL_HAT_DOWN) {
				_pi.dirMask |= PlayerInput::DIR_DOWN;
			}
			if (ev.jhat.value & SDL_HAT_LEFT) {
				_pi.dirMask |= PlayerInput::DIR_LEFT;
			}
			if (ev.jhat.value & SDL_HAT_RIGHT) {
				_pi.dirMask |= PlayerInput::DIR_RIGHT;
			}
			break;
		  //joystick
                case SDL_JOYAXISMOTION:
			switch (ev.jaxis.axis) {

			case 0:
				if (ev.jaxis.value > JOYSTICK_COMMIT_VALUE) {
					_pi.dirMask |= PlayerInput::DIR_RIGHT;
					if (_pi.dirMask & PlayerInput::DIR_LEFT) {
						_pi.dirMask &= ~PlayerInput::DIR_LEFT;
					}
				} else if (ev.jaxis.value < -JOYSTICK_COMMIT_VALUE) {
					_pi.dirMask |= PlayerInput::DIR_LEFT;
					if (_pi.dirMask & PlayerInput::DIR_RIGHT) {
						_pi.dirMask &= ~PlayerInput::DIR_RIGHT;
					}
				} else {
					_pi.dirMask &= ~(PlayerInput::DIR_RIGHT | PlayerInput::DIR_LEFT);
				}
				break;
			case 1:
				if (ev.jaxis.value > JOYSTICK_COMMIT_VALUE) {
					_pi.dirMask |= PlayerInput::DIR_DOWN;
					if (_pi.dirMask & PlayerInput::DIR_UP) {
						_pi.dirMask &= ~PlayerInput::DIR_UP;
					}
				} else if (ev.jaxis.value < -JOYSTICK_COMMIT_VALUE) {
					_pi.dirMask |= PlayerInput::DIR_UP;
					if (_pi.dirMask & PlayerInput::DIR_DOWN) {
						_pi.dirMask &= ~PlayerInput::DIR_DOWN;
					}
				} else {
					_pi.dirMask = 0;
				}
				break;
			}
			break;
	#ifdef TOS
               case SDL_JOYBUTTONDOWN:
			switch (ev.jbutton.button) {
                          case JP_KP1:
                         //scaler -
                         break;
                         case JP_KP2:
                         //scaler +

                         break;
                          case JP_KP3:
                          //take screenshot
                           //do nothing only on button up
                         break;
                          case JP_PAUSE:
                          //escape pressed / PAUSE on JagPad
                          _pi.escape = true;
                          break;
                          case JP_OPTION:
                           //backspace pressed / OPTION on JagPad
                          _pi.backspace = true;
                          break;

                          case JP_FIRE0:
                          _pi.shift = true;
                          break;

                          case JP_FIRE1:
                          //space pressed / FIRE B on JagPad
                       	  _pi.space = true;
                          break;

                          case JP_FIRE2:
                          _pi.enter = true;
                          break;
                        }
			break;
                case SDL_JOYBUTTONUP:
			switch (ev.jbutton.button){
                         case JP_KP1: {
                         //scaler -
                          uint8 s1 = _scaler - 1;
			 if (s1 < NUM_SCALERS)
                            { switchGfxMode(_fullscreen, s1);}
                         }

                         break;
                         case JP_KP2:{
                         //scaler +
                         uint8 s2 = _scaler + 1;
			 if (s2 < NUM_SCALERS) { switchGfxMode(_fullscreen, s2);}
                         }
                         break;
                          case JP_KP3:
                          //take screenshot
                           makeScreenshot();
                         break;

                         case JP_PAUSE:
                         //escape depressed / PAUSE on JagPad
                         _pi.escape = false;
                         break;

                         case JP_OPTION:
                         //backspace depressed / OPTION on JagPad
                         _pi.backspace = false;
                         break;

                         case JP_FIRE0:
                         _pi.shift = false;
                         break;

                         case JP_FIRE1:
                         //space depressed / FIRE B on JagPad
                       	 _pi.space = false;
                         break;

                         case JP_FIRE2:
                         _pi.enter = false;
                         break;

			}
			break;
        #else
               case SDL_JOYBUTTONDOWN:
			switch (ev.jbutton.button) {
			case 0:
				_pi.space = true;
				break;
			case 1:
			    _pi.shift = true;
			    break;
			case 2:
			    _pi.enter = true;
			    break;
			case 3:
			    _pi.backspace = true;
			    break;
			}
			break;
		case SDL_JOYBUTTONUP:
			switch (ev.jbutton.button) {
			case 0:
				_pi.space = false;
				break;
			case 1:
			    _pi.shift = false;
			    break;
			case 2:
			    _pi.enter = false;
			    break;
			case 3:
			    _pi.backspace = false;
			    break;
			}
			break;
		#endif

		case SDL_KEYUP:
			switch (ev.key.keysym.sym) {
			case SDLK_LEFT:
				_pi.dirMask &= ~PlayerInput::DIR_LEFT;
				break;
			case SDLK_RIGHT:
				_pi.dirMask &= ~PlayerInput::DIR_RIGHT;
				break;
			case SDLK_UP:
				_pi.dirMask &= ~PlayerInput::DIR_UP;
				break;
			case SDLK_DOWN:
				_pi.dirMask &= ~PlayerInput::DIR_DOWN;
				break;
			case SDLK_SPACE:
				_pi.space = false;
				break;
			case SDLK_RSHIFT:
			case SDLK_LSHIFT:
				_pi.shift = false;
				break;
			case SDLK_RETURN:
				_pi.enter = false;
				break;
			case SDLK_ESCAPE:
				_pi.escape = false;
				break;
			//make screenshot
                        case SDLK_F10:
			     makeScreenshot();
				break;
			default:
				break;
			}
			break;
                case SDL_KEYDOWN:
                    if (ev.key.keysym.mod &  KMOD_ALT ) {

				if (ev.key.keysym.sym == SDLK_RETURN)
                                   {
                                     switchGfxMode(!_fullscreen, _scaler);
                                   }
                                    #ifdef TOS
                                    else if (ev.key.keysym.sym == SDLK_2)
                                     {
					uint8 s = _scaler + 1;
					if (s < NUM_SCALERS) { switchGfxMode(_fullscreen, s);}
				     } 
                                      else if (ev.key.keysym.sym == SDLK_1)
                                       {
					int8 s = _scaler - 1;
					if (_scaler > 0) {switchGfxMode(_fullscreen, s);}
                                    #else
                                   else if (ev.key.keysym.sym == SDLK_KP_PLUS)
                                     {
					uint8 s = _scaler + 1;
					if (s < NUM_SCALERS) { switchGfxMode(_fullscreen, s);}
				     } else if (ev.key.keysym.sym == SDLK_KP_MINUS)
                                       {
					int8 s = _scaler - 1;
					if (_scaler > 0) {switchGfxMode(_fullscreen, s);}
					#endif
				}
				break;
			} else if (ev.key.keysym.mod & KMOD_CTRL)
                               {
				  if (ev.key.keysym.sym == SDLK_f) {
					_pi.dbgMask ^= PlayerInput::DF_FASTMODE;
				} else if (ev.key.keysym.sym == SDLK_b) {
					_pi.dbgMask ^= PlayerInput::DF_DBLOCKS;
				} else if (ev.key.keysym.sym == SDLK_i) {
					_pi.dbgMask ^= PlayerInput::DF_SETLIFE;
				} else if (ev.key.keysym.sym == SDLK_m) {
					_pi.mirrorMode = !_pi.mirrorMode;
					flipGfx();
				} else if (ev.key.keysym.sym == SDLK_s) {
					_pi.save = true;
				} else if (ev.key.keysym.sym == SDLK_l) {
					_pi.load = true;
				} else if (ev.key.keysym.sym == SDLK_KP_PLUS) {
					_pi.stateSlot = 1;
				} else if (ev.key.keysym.sym == SDLK_KP_MINUS) {
					_pi.stateSlot = -1;
				} else if (ev.key.keysym.sym == SDLK_r) {
					_pi.inpRecord = true;
				} else if (ev.key.keysym.sym == SDLK_p) {
					_pi.inpReplay = true;
				}
			}
			_pi.lastChar = ev.key.keysym.sym;

                        switch (ev.key.keysym.sym) {
			case SDLK_LEFT:
				_pi.dirMask |= PlayerInput::DIR_LEFT;
				break;
			case SDLK_RIGHT:
				_pi.dirMask |= PlayerInput::DIR_RIGHT;
				break;
			case SDLK_UP:
				_pi.dirMask |= PlayerInput::DIR_UP;
				break;
			case SDLK_DOWN:
				_pi.dirMask |= PlayerInput::DIR_DOWN;
				break;
			case SDLK_BACKSPACE:
			case SDLK_TAB:
				_pi.backspace = true;
				break;
			case SDLK_SPACE:
				_pi.space = true;
				break;
			case SDLK_RSHIFT:
			case SDLK_LSHIFT:
				_pi.shift = true;
				break;
			case SDLK_RETURN:
				_pi.enter = true;
				break;
			case SDLK_ESCAPE:
				_pi.escape = true;
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}
}

void SystemStub::sleep(uint32 duration) {
	SDL_Delay(duration);
}

uint32 SystemStub::getTimeStamp() {
	return SDL_GetTicks();
}

void SystemStub::startAudio(AudioCallback callback, void *param) {

       pDesired=(SDL_AudioSpec*)malloc(sizeof(SDL_AudioSpec));
       pObtained=(SDL_AudioSpec*)malloc(sizeof(SDL_AudioSpec));

	memset(pDesired, 0, sizeof(SDL_AudioSpec));
	memset(pObtained, 0, sizeof(SDL_AudioSpec));

	pDesired->freq = SOUND_SAMPLE_RATE;
	pDesired->format = AUDIO_S8;
	pDesired->channels = 1;
	pDesired->samples = 2048;
	pDesired->callback = callback;
	pDesired->userdata = param;

        if (SDL_OpenAudio(pDesired, NULL) == 0) {
		SDL_PauseAudio(0);
	} else {
	  error("SystemStub::startAudio() Unable to open sound device");
	}
}

void SystemStub::stopAudio() {
	SDL_CloseAudio();
}

uint32 SystemStub::getOutputSampleRate() {
	return SOUND_SAMPLE_RATE;
}

void *SystemStub::createMutex() {
	return SDL_CreateMutex();
}

void SystemStub::destroyMutex(void *mutex) {
	SDL_DestroyMutex((SDL_mutex *)mutex);
}

void SystemStub::lockMutex(void *mutex) {
	SDL_mutexP((SDL_mutex *)mutex);
}

void SystemStub::unlockMutex(void *mutex) {
	SDL_mutexV((SDL_mutex *)mutex);
}

void SystemStub::prepareGfxMode() {
	int w = _screenW * _scalers[_scaler].factor;
	int h = _screenH * _scalers[_scaler].factor;
	#ifdef TOS
	_screen = SDL_SetVideoMode(w, h, 16, _fullscreen ? (SDL_FULLSCREEN | SDL_HWSURFACE) : SDL_HWSURFACE);
	#else
	 _screen = SDL_SetVideoMode(w, h, 16, _fullscreen ? (SDL_FULLSCREEN | SDL_HWSURFACE) : SDL_HWSURFACE);
        #endif
	if (!_screen) {
		error("SystemStub::prepareGfxMode() Unable to allocate _screen buffer");
	}
	const SDL_PixelFormat *pf = _screen->format;
	#ifdef TOS
	_sclscreen = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 16, pf->Rmask, pf->Gmask, pf->Bmask, pf->Amask);
	
	#else
	  _sclscreen = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 16, pf->Rmask, pf->Gmask, pf->Bmask, pf->Amask);

	#endif
	if (!_sclscreen) {
		error("SystemStub::prepareGfxMode() Unable to allocate _sclscreen buffer");
	}
	forceGfxRedraw();
}

void SystemStub::cleanupGfxMode() {
	if (_offscreen) {
          #ifdef TOS
          Mfree(_offscreen);
          #else
		free(_offscreen);
          #endif
		_offscreen = 0;
	}
	if (_sclscreen) {
		SDL_FreeSurface(_sclscreen);
		_sclscreen = 0;
	}
	if (_screen) {
		// freed by SDL_Quit()
		_screen = 0;
	}
}

void SystemStub::switchGfxMode(bool fullscreen, uint8 scaler) {
	SDL_Surface *prev_sclscreen = _sclscreen;
	SDL_FreeSurface(_screen);
	_fullscreen = fullscreen;
	_scaler = scaler;
	prepareGfxMode();
	SDL_BlitSurface(prev_sclscreen, NULL, _sclscreen, NULL);
	SDL_FreeSurface(prev_sclscreen);
}

void SystemStub::flipGfx() {
	uint16 scanline[256];
	assert(_screenW <= 256);
	uint16 *p = (uint16 *)_offscreen + _screenW + 1;
	for (int y = 0; y < _screenH; ++y) {
		p += _screenW;
		for (int x = 0; x < _screenW; ++x) {
			scanline[x] = *--p;
		}
		memcpy(p, scanline, _screenW * sizeof(uint16));
		p += _screenW;
	}
	forceGfxRedraw();
}

void SystemStub::forceGfxRedraw() {
	_numBlitRects = 1;
	_blitRects[0].x = 0;
	_blitRects[0].y = 0;
	_blitRects[0].w = _screenW;
	_blitRects[0].h = _screenH;
}

void SystemStub::drawRect(SDL_Rect *rect, uint8 color, uint16 *dst, uint16 dstPitch) {
	dstPitch >>= 1;
	int x1 = rect->x;
	int y1 = rect->y;
	int x2 = rect->x + rect->w - 1;
	int y2 = rect->y + rect->h - 1;
	assert(x1 >= 0 && x2 < _screenW && y1 >= 0 && y2 < _screenH);
	for (int i = x1; i <= x2; ++i) {
		*(dst + y1 * dstPitch + i) = *(dst + y2 * dstPitch + i) = _pal[color];
	}
	for (int j = y1; j <= y2; ++j) {
		*(dst + j * dstPitch + x1) = *(dst + j * dstPitch + x2) = _pal[color];
	}
}

void SystemStub::makeScreenshot(void)
{
  char filename[16]={0};
  char path[128]={0};
  sprintf ( filename, "S%d.TGA", _iNumScreenshots );

  strcpy (path,SCREENSHOTPATH);
  strcat (path,filename);
  //make PNG screenshot and save it to specified path in BMP format
   SaveTGA(_screen, path, 1);
   _iNumScreenshots=_iNumScreenshots+1;
   /*
     if((SDL_SaveBMP (_screen, path)!=0)){_iNumScreenshots=_iNumScreenshots+1; }
     else {debug(DBG_INFO,"Error: cannot save screenshot: %s \n", path); }  */
}

int SystemStub::openJoystick(int iID, SDL_Joystick *pJoyStruct)
{
 pJoyStruct = SDL_JoystickOpen(iID);

     debug(DBG_INFO,"*****    %s\n", SDL_JoystickName(iID));


     if(pJoyStruct!=NULL)
     {   //check characteristics
       debug(DBG_INFO,"Number of buttons: %d\n", SDL_JoystickNumButtons(pJoyStruct)) ;
       debug(DBG_INFO,"Number of joy axes: %d\n",SDL_JoystickNumAxes(pJoyStruct));
       debug(DBG_INFO,"Number of joy balls: %d\n",SDL_JoystickNumBalls(pJoyStruct));
       debug(DBG_INFO,"Number of joy hats: %d\n",SDL_JoystickNumHats(pJoyStruct));

       return 1;
     }
     else
                    return 0;



}


