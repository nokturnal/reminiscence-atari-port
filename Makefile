#PREFIX=
PREFIX=/usr/m68k-atari-mint/bin/
PROJECT_NAME=REminiscence-0.1.9

SDL_CFLAGS = `${PREFIX}sdl-config --cflags`
SDL_LIBS = `${PREFIX}sdl-config --libs`


# sound defines:
# -DNOMUSIC - disables mod player -> no music at all
# -DMOD_DSPMOD - uses DSP mod 3.4 player

#DEFINES = -DBYPASS_PROTECTION
DEFINES = -DBYPASS_PROTECTION -DTOS -DNDEBUG

CXX = m68k-atari-mint-g++
CXXFLAGS:= -g -O2 -m68020-60 -Wall -pedantic -ansi -fsigned-char -Wuninitialized -Wno-unknown-pragmas -Wshadow -Wimplicit
CXXFLAGS+= -Wundef -Wreorder -Wwrite-strings -Wnon-virtual-dtor -Wno-multichar -fomit-frame-pointer -fno-exceptions -fno-rtti
CXXFLAGS+= $(SDL_CFLAGS) $(DEFINES)

UPX = upx
create_dirs:
	if [ -d ./bin ];then echo "./bin directory exists, do nothing.";else \
		echo "./bin directory doesn't exist, creating new one."; \
		mkdir ./bin; \
	fi
	if [ -d ./obj ];then echo "./obj directory exists, do nothing.";else \
		echo "./obj directory doesn't exist, creating new one."; \
		mkdir ./obj; \
	fi
	if [ -d ./bin_dist ];then echo " ./bin_dist directory exists, do nothing."; \
	else \
		echo "./bin_dist directory doesn't exist, creating new one."; \
		mkdir ./bin_dist; \
	fi

	if [ -d ./archive_dist ];then echo "./archive_dist directory exists, do nothing."; \
	else \
		echo "./archive_dist directory doesn't exist, creating new one."; \
		mkdir ./archive_dist;\
	fi

SRCS = collision.cpp cutscene.cpp file.cpp game.cpp graphics.cpp main.cpp menu.cpp \
	mixer.cpp mod_player.cpp piege.cpp resource.cpp scaler.cpp sfx_player.cpp \
	staticres.cpp systemstub.cpp unpack.cpp util.cpp video.cpp save_tga.cpp

OBJS = $(SRCS:.cpp=.o)
DEPS = $(SRCS:.cpp=.d)

OBJL= ./obj/collision.o ./obj/cutscene.o ./obj/file.o ./obj/game.o ./obj/graphics.o ./obj/main.o ./obj/menu.o \
	./obj/mixer.o ./obj/mod_player.o ./obj/piege.o ./obj/resource.o ./obj/scaler.o ./obj/sfx_player.o \
	./obj/staticres.o ./obj/systemstub.o ./obj/unpack.o ./obj/util.o ./obj/video.o ./obj/save_tga.o
	
rsnp.ttp: create_dirs  $(OBJS)
	$(CXX) $(LDFLAGS) -o ./bin/$@ $(OBJL) $(SDL_LIBS) -lz -s

.cpp.o:
	$(CXX) $(CXXFLAGS) -MMD -c $< -o ./obj/$*.o

final: rsnp.ttp clean_packed
	m68k-atari-mint-strip ./bin/rsnp.ttp
	upx -v --best -o ./bin/rs.ttp ./bin/rsnp.ttp

clean:  
	rm -f ./obj/*.o ./obj/*.d 
	rm -f ./bin/*.ttp 
	rm -rf ./bin
	rm  -rf ./obj
	rm  -rf ./bin_dist
	rm  -rf ./archive_dist

create_bin_dist:
	mkdir ./bin_dist/DATA
	mkdir ./bin_dist/DATA/VOICE
	mkdir ./bin_dist/DATA/MODS
	mkdir ./bin_dist/SCRNSHOT.DIR
	cp ./DATA.DIR ./bin_dist/DATA
	cp ./MOD.DIR ./bin_dist/DATA/MODS
	cp ./VOICE.DIR ./bin_dist/DATA/VOICE
	cp ./SCRNSHOT.DIR ./bin_dist/SCRNSHOT.DIR
	cp ./bin/rs.ttp ./bin_dist
	cp ./COPYING ./bin_dist
	cp ./README ./bin_dist
	cp ./READ_ME.NFO ./bin_dist
	if -f "${PROJECT_NAME}-atari-port_bin.tar.bz2";then \
		echo "Binary archive exists, replacing it with the new one ..."; \
	rm -f "${PROJECT_NAME}-atari-port_bin.tar.bz2"; fi;
	tar -cjvf "${PROJECT_NAME}-atari-port_bin.tar.bz2" ./bin_dist/*
	##mv "${PROJECT_NAME}-atari-port_bin.tar.bz2" ./bin_dist

create_src_archive:
	echo "Creating source archive distribution ..."
	echo "${PROJECT_NAME}-atari-port_src.tar.bz2";
	if -f "${PROJECT_NAME}-atari-port_src.tar.bz2";then \
	echo "Source archive exists, replacing it with the new one ..."; \
	rm -f "${PROJECT_NAME}-atari-port_src.tar.bz2"; \
	fi
	tar -cjvf "./${PROJECT_NAME}-atari-port_src.tar.bz2" *
	mv "./${PROJECT_NAME}-atari-port_src.tar.bz2" ./archive_dist
clean_packed: 
	rm -f ./bin/rs.ttp

all: clean create_dirs create_src_archive final create_bin_dist

-include $(DEPS)

