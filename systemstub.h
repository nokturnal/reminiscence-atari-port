/* REminiscence - Flashback interpreter
 * Copyright (C) 2005-2007 Gregory Montoir
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __SYSTEMSTUB_H__
#define __SYSTEMSTUB_H__

#include "intern.h"
#include <SDL.h>

#ifdef TOS
#include <mint/osbind.h>

#define SCALER_INI 0
#define FULLSCREEN true
#define MAX_JOY 17

#else
#define SCALER_INI 0
#define FULLSCREEN false
#define MAX_JOY 1
#endif

struct PlayerInput {
	enum {
		DIR_UP    = 1 << 0,
		DIR_DOWN  = 1 << 1,
		DIR_LEFT  = 1 << 2,
		DIR_RIGHT = 1 << 3
	};
	enum {
		DF_FASTMODE = 1 << 0,
		DF_DBLOCKS  = 1 << 1,
		DF_SETLIFE  = 1 << 2
	};

	uint8 dirMask;
	bool enter;
	bool space;
	bool shift;
	bool backspace;
	bool escape;

	char lastChar;

	bool save;
	bool load;
	int stateSlot;

	bool inpRecord;
	bool inpReplay;

	bool mirrorMode;

	uint8 dbgMask;
	bool quit;
};

struct SystemStub  {

  typedef void (*AudioCallback)(void *param, uint8 *stream, int len);
       
  enum {
	MAX_BLIT_RECTS = 200,
#ifdef TOS
	SOUND_SAMPLE_RATE = 11025,
	JOYSTICK_COMMIT_VALUE = 3200 	};
#else
	SOUND_SAMPLE_RATE = 11025,
	JOYSTICK_COMMIT_VALUE = 3200  	};
#endif

	uint8 *_offscreen;
	SDL_Surface *_screen;
	SDL_Surface *_sclscreen;
	bool _fullscreen;
	uint8 _scaler;
	uint8 _overscanColor;
	
	uint16 _pal[256];
	uint16 _screenW, _screenH;
        uint16 iNumJoysticks;
	uint16 _numBlitRects;
        uint16 _iNumScreenshots;

	SDL_Joystick *_joystick[MAX_JOY];
	SDL_Rect _blitRects[MAX_BLIT_RECTS];
	
	SDL_AudioSpec *pDesired;
	SDL_AudioSpec *pObtained;
	PlayerInput _pi;
	
	void init(const char *title, uint16 w, uint16 h);
	void destroy();
	void setPalette(const uint8 *pal, uint16 n);
	void setPaletteEntry(uint8 i, const Color *c);
	void getPaletteEntry(uint8 i, Color *c);
	void setOverscanColor(uint8 i);
	void copyRect(int16 x, int16 y, uint16 w, uint16 h, const uint8 *buf, uint32 pitch);
	void updateScreen(uint8 shakeOffset);
	void processEvents();
	void sleep(uint32 duration);
	uint32 getTimeStamp();
	void startAudio(AudioCallback callback, void *param);
	void stopAudio();
	uint32 getOutputSampleRate();
	void *createMutex();
	void destroyMutex(void *mutex);
	void lockMutex(void *mutex);
	void unlockMutex(void *mutex);
        void makeScreenshot(void) ;
	int openJoystick(int iID, SDL_Joystick *pJoyStruct);
	void prepareGfxMode();
	void cleanupGfxMode();
	void switchGfxMode(bool fullscreen, uint8 scaler);
	void flipGfx();
	void forceGfxRedraw();
	void drawRect(SDL_Rect *rect, uint8 color, uint16 *dst, uint16 dstPitch);
};

struct MutexStack {
	SystemStub *_stub;
	void *_mutex;

	MutexStack(SystemStub *stub, void *mutex):_stub(stub), _mutex(mutex) {
	  _stub->lockMutex(_mutex);
	}
	~MutexStack() {
	  _stub->unlockMutex(_mutex);
	}
};

#endif // __SYSTEMSTUB_H__
